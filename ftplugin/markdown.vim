" Vim filetype plugin file
" Language:	Markdown
" Maintainer: Albin Ahlbäck <albin.ahlback@gmail.com>
" Last Change: 25th of February, 2021

if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

let b:balsas_loaded = 1

inoremap <F2> <C-c>:call RecordVoice()<CR>
nnoremap <F2> :call RecordVoice()<CR>

function! RecordVoice()
    let ln = getline(line("."))
    let ix = match(ln, " | ")

    if ix == -1
        echomsg 'Could not match " | ".'
        finish
    endif

    let ln = tolower(ln[ix+3:-1])
    let lt_chars = ["ą", "č", "ę", "ė", "į", "š", "ų", "ū", "ž"]
    let en_chars = ["a", "c", "e", "e", "i", "s", "u", "u", "z"]

    " Convert to ASCII
    let ix = -1
    for char in lt_chars
        let ix += 1
        while 1
            let iy = match(ln, char)
            if iy == -1
                break
            else
                let ln = ln[0:iy-1].en_chars[ix].ln[iy+2:-1]
            end
        endwhile
    endfor

    " Remove double spaces
    while 1
        let ix = match(ln, "  ")
        if ix == -1
            break
        else
            let ln = ln[0:ix].ln[ix+2:-1]
        end
    endwhile

    " Remove whitespaces at the ends
    while ln[0] == " " || ln[-1] == '\t'
        let ln = ln[1:-1]
    endwhile
    while ln[-1] == " " || ln[-1] == '\t'
        let ln = ln[0:-2]
    endwhile

    " Convert whitespaces to underscore
    for ix in range(0, len(ln) - 1)
        if ln[ix] == " " || ln[ix] == '\t'
            let ln[ix] = "_"
        endif
    endfor

    " Get path
    let currentpath = expand('%:p:h')

    " Create a folder for recordings if it doesn't exist
    if !isdirectory(currentpath.'/recordings')
        exe '! mkdir '.currentpath.'/recordings'
    endif

    " Record
    exe '! sox -v 15.0 -d '.currentpath.'/recordings/'.ln.'.wav channels 1'
endfunction
